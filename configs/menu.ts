const adminMenu = [{
    pagename: "home",
    image: "/assets/img/home.svg",
    href: "/home",
    show: true
}, {
    pagename: "schools",
    image: "/assets/img/schools.svg",
    href: "/schools",
    show: true
}, {
    pagename: "staffs",
    image: "/assets/img/staff.svg",
    href: "/staffs",
    show: true,
    popup: 'school staff'
}, {
    pagename: "students",
    image: "/assets/img/student.svg",
    href: "/students",
    show: true
}, {
    pagename: "parents",
    image: "/assets/img/parent.svg",
    href: "/parents",
    show: true
}, {
    pagename: "groups",
    image: "/assets/img/conversations.svg",
    href: "/groups",
    show: true,
    popup: 'discussion board'
}, {
    pagename: "conversations",
    image: "/assets/img/messages.svg",
    href: "/conversations",
    show: true,
    popup: 'messages'
}, {
    pagename: "subscriptions",
    image: "/assets/img/subscriptions.svg",
    href: "/subscribers",
    show: true
}, {
    pagename: "quizes",
    image: "/assets/img/quiz.svg",
    href: "/quizes",
    show: true,
    popup: "quizzes"
}, {
    pagename: "rankings",
    image: "/assets/img/rankings.svg",
    href: "/rankings",
    show: true
}, {
    pagename: "admins",
    image: "/assets/img/settings.svg",
    href: "/admins",
    show: true
}];

const schoolMenu = [{
    pagename: "home",
    image: "/assets/img/home.svg",
    href: "/home",
    show: true
}, {
    pagename: "staffs",
    image: "/assets/img/staff.svg",
    href: "/staffs",
    show: true,
    popup: 'school staff'
}, {
    pagename: "students",
    image: "/assets/img/student.svg",
    href: "/students",
    show: true
}, {
    pagename: "parents",
    image: "/assets/img/parent.svg",
    href: "/parents",
    show: true
}, {
    pagename: "groups",
    image: "/assets/img/conversations.svg",
    href: "/groups",
    show: true,
    popup: 'discussion board'
}, {
    pagename: "conversations",
    image: "/assets/img/messages.svg",
    href: "/conversations",
    show: true,
    popup: 'messages'
}, {
    pagename: "subscriptions",
    image: "/assets/img/subscriptions.svg",
    href: "/subscribers",
    show: true
}, {
    pagename: "quizes",
    image: "/assets/img/quiz.svg",
    href: "/quizes",
    show: true,
    popup: 'quizzes'
}, {
    pagename: "rankings",
    image: "/assets/img/rankings.svg",
    href: "/rankings",
    show: true
}];

const staffMenu = [{
    pagename: "home",
    image: "/assets/img/home.svg",
    href: "/home",
    show: true
}, {
    pagename: "parents",
    image: "/assets/img/parent.svg",
    href: "/parents",
    show: true
}, {
    pagename: "students",
    image: "/assets/img/student.svg",
    href: "/students",
    show: true
}, {
    pagename: "groups",
    image: "/assets/img/conversations.svg",
    href: "/groups",
    show: true,
    popup: 'discussion board'
}, {
    pagename: "conversations",
    image: "/assets/img/messages.svg",
    href: "/conversations",
    show: true,
    popup: 'messages'
}, {
    pagename: "quizes",
    image: "/assets/img/quiz.svg",
    href: "/quizes",
    show: true,
    popup: 'quizzes'
}, {
    pagename: "rankings",
    image: "/assets/img/rankings.svg",
    href: "/rankings",
    show: true
}];

const studentsMenu = [{
    pagename: "home",
    image: "/assets/img/home.svg",
    href: "/home",
    show: true
}, {
    pagename: "groups",
    image: "/assets/img/conversations.svg",
    href: "/groups",
    show: true,
    popup: 'discussion board'
}, {
    pagename: "conversations",
    image: "/assets/img/messages.svg",
    href: "/conversations",
    show: true,
    popup: 'messages'
}, {
    pagename: "quizes",
    image: "/assets/img/quiz.svg",
    href: "/student-quizes",
    show: true,
    popup: 'quizzes'
}, {
    pagename: "rankings",
    image: "/assets/img/rankings.svg",
    href: "/rankings",
    show: true
}];

const parentsMenu = [{
    pagename: "home",
    image: "/assets/img/home.svg",
    href: "/home",
    show: true
}, {
    pagename: "students",
    image: "/assets/img/student.svg",
    href: "/students",
    show: true,
    popup: 'wards'
}, {
    pagename: "groups",
    image: "/assets/img/conversations.svg",
    href: "/groups",
    show: true,
    popup: 'discussion board'
}, {
    pagename: "conversations",
    image: "/assets/img/messages.svg",
    href: "/conversations",
    show: true,
    popup: 'messages'
}, {
    pagename: "rankings",
    image: "/assets/img/rankings.svg",
    href: "/rankings",
    show: true
}];

const defaultMenu = [{
    pagename: "home",
    image: "/assets/img/home.svg",
    href: "/home",
    show: true
}, {
    pagename: "feedback",
    image: "/assets/img/feedback.svg",
    href: "/blank",
    show: true
}, {
    pagename: "help",
    image: "/assets/img/help.svg",
    href: "/blank",
    show: true
}]

export {
    adminMenu,
    schoolMenu,
    staffMenu,
    studentsMenu,
    parentsMenu,
    defaultMenu
};